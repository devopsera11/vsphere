# Configure the AWS provider
provider "aws" {
  access_key = "<your-access-key>"
  secret_key = "<your-secret-key>"
  region     = "<your-region>"
}

# Create a security group for the load balancer
resource "aws_security_group" "lb_security_group" {
  name        = "lb-security-group"
  description = "Security group for load balancer"
  
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create an ELB
resource "aws_elb" "my_elb" {
  name               = "my-elb"
  security_groups    = [aws_security_group.lb_security_group.id]
  subnets            = [aws_subnet.public_subnet.id]
  
  listener {
    instance_port     = 80
    instance_protocol = "HTTP"
    lb_port           = 80
    lb_protocol       = "HTTP"
  }
  
  health_check {
    target             = "HTTP:80/"
    interval           = 30
    healthy_threshold  = 2
    unhealthy_threshold = 2
    timeout            = 5
  }
}

# Create instances or target groups to attach to the ELB
resource "aws_instance" "my_instance" {
  # Configuration for your instance(s)
}

# Attach instances to the ELB
resource "aws_elb_attachment" "my_elb_attachment" {
  elb        = aws_elb.my_elb.name
  instance   = aws_instance.my_instance.id
  attachment = {
    instance_id = aws_instance.my_instance.id
  }
}

# Configure the AWS provider
provider "aws" {
  access_key = "<your-access-key>"
  secret_key = "<your-secret-key>"
  region     = "<your-region>"
}

# Create a network firewall
resource "aws_networkfirewall_firewall" "my_firewall" {
  name        = "my-firewall"
  description = "Network Firewall for Magnolia CMS"

  subnet_mapping {
    subnet_id = aws_subnet.public_subnet.id
  }

  firewall_policy_arn = "<your-firewall-policy-arn>"

  # Define the rule groups
  rule_group_references {
    priority          = 1
    rule_group_arn    = "<your-rule-group-arn>"
    override_priority = 50
    override_action   = "alert"
  }

  # Define additional rule groups if needed
  # rule_group_references {
  #   priority          = 2
  #   rule_group_arn    = "<additional-rule-group-arn>"
  #   override_priority = 50
  #   override_action   = "alert"
  # }
}

# Create a WAF web ACL
resource "aws_wafv2_web_acl" "my_web_acl" {
  name        = "my-web-acl"
  description = "WAF Web ACL for Magnolia CMS"
  scope       = "REGIONAL"

  default_action {
    allow {}
  }

  # Define rules for the WAF ACL
  rule {
    name        = "AllowGoodBots"
    priority    = 0
    action {
      allow {}
    }
    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesBotControlRuleSet"
        vendor_name = "AWS"
      }
    }
    visibility_config {
      sampled_requests_enabled = true
      cloudwatch_metrics_enabled = true
    }
  }

  # Define additional rules if needed
  # rule {
  #   # ...
  # }
}

# Configure the AWS provider
provider "aws" {
  access_key = "<your-access-key>"
  secret_key = "<your-secret-key>"
  region     = "<your-region>"
}

# Create an S3 bucket to store the static assets
resource "aws_s3_bucket" "my_bucket" {
  bucket = "my-bucket"
  acl    = "private"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  # Enable versioning for the bucket if needed
  # versioning {
  #   enabled = true
  # }
}

# Create a CloudFront distribution
resource "aws_cloudfront_distribution" "my_distribution" {
  enabled             = true
  is_ipv6_enabled     = true
  comment             = "CDN for Magnolia CMS"
  default_root_object = "index.html"

  origin {
    domain_name = aws_s3_bucket.my_bucket.website_endpoint
    origin_id   = "S3Origin"
  }

  default_cache_behavior {
    target_origin_id = "S3Origin"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  # Add additional cache behaviors if needed
  # cache_behavior {
  #   # ...
  # }
}

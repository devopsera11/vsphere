provider "aws" {
  region = "us-west-2"  # Replace with your desired AWS region
}

resource "aws_s3_bucket" "terraform_state_bucket" {
  bucket = "terraform-state-bucket-example"  # Replace with your desired bucket name
  acl    = "private"
}

resource "aws_cloudwatch_event_rule" "terraform_state_backup_rule" {
  name        = "terraform-state-backup-rule"
  description = "Triggering the Terraform state backup every 5 minutes"

  schedule_expression = "rate(5 minutes)"
}

resource "aws_cloudwatch_event_target" "terraform_state_backup_target" {
  rule      = aws_cloudwatch_event_rule.terraform_state_backup_rule.name
  arn       = aws_s3_bucket.terraform_state_bucket.arn
  target_id = "terraform-state-backup-target"

  input = <<EOF
{
  "source": ["aws.ec2"],
  "detail-type": ["AWS API Call via CloudTrail"],
  "detail": {
    "eventSource": ["s3.amazonaws.com"],
    "eventName": ["PutObject"]
  }
}
EOF
}

output "bucket_name" {
  value = aws_s3_bucket.terraform_state_bucket.id
}

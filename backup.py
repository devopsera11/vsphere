import shutil
import time

def backup_terraform_state():
    timestamp = time.strftime("%Y%m%d%H%M%S")
    state_file = "/path/to/terraform.tfstate"
    backup_file = f"/path/to/backup/terraform.tfstate_{timestamp}"
    
    shutil.copy2(state_file, backup_file)
    print(f"Backup created: {backup_file}")

if __name__ == "__main__":
    while True:
        backup_terraform_state()
        time.sleep(300)  # Sleep for 5 minutes (300 seconds)

# vsphere

## Getting started


```
terraform init
terraform plan
terraform plan -target=vsphere_virtual_machine.caching_server
terraform apply
terraform apply -target=vsphere_virtual_machine.caching_server
```


```
cd existing_repo
git remote add origin https://gitlab.com/devopsera11/vsphere.git
git branch -M main
git push -uf origin main
```

***

## Authors and acknowledgment
Dimitrios Sakellaropoulos, DevOps
